$(document).ready ->
  menuToggle = $('#js-centered-navigation-mobile-menu').unbind()
  $('#js-centered-navigation-menu').removeClass 'show'
  menuToggle.on 'click', (e) ->
    e.preventDefault()
    $('#js-centered-navigation-menu').slideToggle ->
      if $('#js-centered-navigation-menu').is(':hidden')
        $('#js-centered-navigation-menu').removeAttr 'style'
      return
    return
  return

$(document).ready ->
  element = document.getElementById('js-fadeInElement')
  $(element).addClass 'js-fade-element-hide'
  $(window).scroll ->
    if $('#js-fadeInElement').length > 0
      elementTopToPageTop = $(element).offset().top
      windowTopToPageTop = $(window).scrollTop()
      windowInnerHeight = window.innerHeight
      elementTopToWindowTop = elementTopToPageTop - windowTopToPageTop
      elementTopToWindowBottom = windowInnerHeight - elementTopToWindowTop
      distanceFromBottomToAppear = 300
      if elementTopToWindowBottom > distanceFromBottomToAppear
        $(element).addClass 'js-fade-element-show'
      else if elementTopToWindowBottom < 0
        $(element).removeClass 'js-fade-element-show'
        $(element).addClass 'js-fade-element-hide'
    return
  return

# scroll with no margin
$(document).ready ->
  logo = $('h1#logo')
  resplogo = $('#resp-logo')
  menu = $('.centered-navigation-menu')
  $(window).scroll ->
    if $(@).width() > 860
      console.log("kurang dari 639")
      if $(this).scrollTop() > 200
        # logo.addClass 'scroll-margin'
        logo.hide()
        resplogo.fadeIn()
        menu.css('text-align','right')
      else
        # logo.removeClass 'scroll-margin'
        logo.show()
        resplogo.fadeOut();
        menu.removeAttr('style')

# aboutus
do (jQuery) ->
  jQuery.mark = jump: (options) ->
    defaults = selector: 'a.scroll-on-page-link'
    if typeof options == 'string'
      defaults.selector = options
    options = jQuery.extend(defaults, options)
    jQuery(options.selector).click (e) ->
      jumpobj = jQuery(this)
      target = jumpobj.attr('href')
      thespeed = 1000
      # 180 small
      # 
      offset = jQuery(target).offset().top - 120
      jQuery('html,body').animate { scrollTop: offset }, thespeed, 'swing'
      e.preventDefault()
      return
  return
jQuery ->
  jQuery.mark.jump()
  return

# accordion
$('.js-accordion-trigger').bind 'click', (e) ->
  jQuery(this).parent().find('.submenu').slideToggle 'fast'
  jQuery(this).parent().toggleClass 'is-expanded'
  e.preventDefault()
  return

$(document).ready ->
  $('.accordion-tabs-minimal').each (index) ->
    $(this).children('li').first().children('a').addClass('is-active').next().addClass('is-open').show()
    return
  $('.accordion-tabs-minimal').on 'click', 'li > a', (event) ->
    if !$(this).hasClass('is-active')
      event.preventDefault()
      accordionTabs = $(this).closest('.accordion-tabs-minimal')
      accordionTabs.find('.is-open').removeClass('is-open').hide()
      $(this).next().toggleClass('is-open').toggle()
      accordionTabs.find('.is-active').removeClass 'is-active'
      $(this).addClass 'is-active'
    else
      event.preventDefault()
    return
  return


$(document).ready ->
  $('.slider-wrap').unslider();

$(document).ready ->
  # $('.panel .header a').preventDefault(e)
  $('.panel .header').click (e)->
    # console.log('diklik')
    e.preventDefault()
    target = $(@).children().data("trigger")
    $(target).slideToggle('fast')


