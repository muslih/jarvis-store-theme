---
layout: page
title: Account
permalink: /account.html
---
<article id="account">
	<section class="detail">
		<h3><i class="fi-credit-card"></i> My Account</h3>
		 <div class="address">
			<address>
			<!-- Your name -->
				<strong>Ashok Ramesh</strong><br>
				<!-- Address -->
				795 Folsom Ave, Suite 600<br>
				San Francisco, CA 94107<br>

				<!-- Phone number -->
				<abbr title="Phone">P:</abbr> (123) 456-7890.<br />
				<a href="mailto:#">first.last@gmail.com</a>
			</address>
		</div>
		<hr>
		<h3><i class="fi-shopping-bag"></i> My Recent Purchases</h3>
		<table class="table-minimal">
			<thead>
			<tr>
			  <th>Date</th>
			  <th>ID</th>
			  <th>Name</th>
			  <th>Price</th>
			  <th>Status</th>
			</tr>
			</thead>
			<tbody>
			<tr>
			  <td>25-08-12</td>
			  <td>4233</td>
			  <td>HTC One</td>
			  <td>$530</td>
			  <td>Completed</td>
			</tr>
			<tr>
				<td>12-02-12</td>
				<td>6643</td>
				<td>Sony Xperia</td>
				<td>$330</td>
				<td>Shipped</td>
			</tr>
			<tr>
				<td>12-0814</td>
				<td>1283</td>
				<td>Nokia Asha</td>
				<td>$230</td>
				<td>Processing</td>
			</tr>
			</tbody>
		</table>


	</section>
</article>



