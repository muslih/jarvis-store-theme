---
layout: blog
title: Blog page
permalink: /blog.html
---

<article>
	<section>
        <h2><a href="blogsingle.html">Sed justo sceleque ut constur sceisque</a></h2>                   
		<!-- Meta details -->
		<div class="meta">
		<i class="fi-calendar"></i> 26-2-2012 <i class="fi-torso"></i> Admin <i class="fi-folder"></i> <a href="#">General</a> <span class="comment"><i class="fi-comment"></i> <a href="#">2 Comments</a></span>
		</div>

		<!-- Thumbnail -->
		<div class="bthumb">
		<a href="#"><img src="./public/img/blog-img.jpg" alt="" class="img-responsive"></a>
		</div>

		<!-- Para -->
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vulputate eros nec odio egestas in dictum nisi vehicula. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse porttitor luctus imperdiet. <a href="#">Praesent ultricies</a> enim ac ipsum aliquet pellentesque. Nullam justo nunc, dignissim at convallis posuere, sodales eu orci. Duis a risus sed dolor placerat semper quis in urna. Nam risus magna, fringilla sit amet blandit viverra, dignissim eget est. Ut <strong>commodo ullamcorper risus nec</strong> mattis. Fusce imperdiet ornare dignissim. Donec aliquet convallis tortor, et placerat quam posuere posuere. Morbi tincidunt posuere turpis eu laoreet. Nulla facilisi. Aenean at massa leo. Vestibulum in varius arcu.</p>

		<!-- Read more -->
		<a href="blogsingle.html" class="btn btn-info">Baca selengkapnya...</a>
	</section>

	<section>
		<h2><a href="blogsingle.html">Sed justo sceleque ut constur sceisque</a></h2>                   
		<!-- Meta details -->
		<div class="meta">
		<i class="fi-calendar"></i> 26-2-2012 <i class="fi-torso"></i> Admin <i class="fi-folder"></i> <a href="#">General</a> <span class="comment"><i class="fi-comment"></i> <a href="#">2 Comments</a></span>
		</div>

		<!-- Para -->
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vulputate eros nec odio egestas in dictum nisi vehicula. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse porttitor luctus imperdiet. <a href="#">Praesent ultricies</a> enim ac ipsum aliquet pellentesque. Nullam justo nunc, dignissim at convallis posuere, sodales eu orci. Duis a risus sed dolor placerat semper quis in urna. Nam risus magna, fringilla sit amet blandit viverra, dignissim eget est. Ut <strong>commodo ullamcorper risus nec</strong> mattis. Fusce imperdiet ornare dignissim. Donec aliquet convallis tortor, et placerat quam posuere posuere. Morbi tincidunt posuere turpis eu laoreet. Nulla facilisi. Aenean at massa leo. Vestibulum in varius arcu.</p>

		<!-- Read more -->
		<a href="blogsingle.html" class="btn btn-info">Baca selengkapnya...</a>
	</section>
	
	<div class="pagination">
	  <ul>
	    <li class="page-prev fleft"><a href="javascript:void(0)">Prev</a></li>
	    
	    <li class="page-next fright" ><a href="javascript:void(0)">Next</a></li>
	  </ul>
	</div>

</article>
{% include aside_blog.html %}