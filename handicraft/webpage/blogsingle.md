---
layout: page
title: Blog <span>Single</span>
permalink: /blogsingle.html
---

<div class="eight columns section">
	<div class="row">
		<article class="twelve columns">
			<h3><a href="#">Mauris est et turpis.</a></h3>
			<div class="meta">
                <i class="icon-calendar"></i> 26-2-2012 <i class="icon-user"></i> Admin <i class="icon-folder-open"></i> <a href="#">General</a> <span class="pull-right"><i class="icon-comment"></i> <a href="#">2 Comments</a></span>
            </div>

			<p>
			<div class="image photo">
				<img src="./public/img/photo.jpg" alt="">
			</div>
			Proin elit arcu, <em class="ttip" data-tooltip="Another tooltip!">rutrum</em> commodo, vehicula tempus, commodo a, risus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est voluptatibus reiciendis, porro officiis. Rerum veritatis optio eius? Deserunt iusto sequi libero illum inventore molestiae assumenda minima, placeat, laborum esse reiciendis. </p>
			<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum facilis nesciunt necessitatibus obcaecati, aliquam harum praesentium, voluptatem voluptates reprehenderit debitis repellendus vel natus nostrum. Dolorum est necessitatibus voluptas, aperiam excepturi.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem fugit quo ad nobis maxime! Obcaecati pariatur quibusdam ut ipsa veniam dolores ullam aliquid maxime, doloremque provident! Exercitationem impedit, quisquam doloremque.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur molestiae esse commodi maiores est, iste minus vel corrupti recusandae neque at, odio facilis, quos laudantium? Culpa nesciunt ex reiciendis aliquid.
			</p>
		</article>
	</div>
	<!-- comment section -->
	<div class="comments">                         
        <div class="title"><h4>2 Comments</h4></div>
        <ul class="comment-list">
			<li class="comment">
			<a class="fleft" href="#">
				<img class="avatar img-responsive" src="./public/img/user.jpg"/>
			</a>
				<div class="comment-author"><a href="#">Ashok</a></div>
				<div class="cmeta">Commented on 25/12/2012</div>
				<p>Nulla facilisi. Sed justo dui, scelerisque ut consectetur vel, eleifend id erat. Phasellus condimentum rutrum aliquet. Quisque eu consectetur erat.</p>
				<div class="clearfix"></div>
			</li>
			<li class="comment reply">
			<a class="fleft" href="#">
				<img class="avatar img-responsive" src="./public/img/user.jpg" />
			</a>
				<div class="comment-author"><a href="#">Ashok</a></div>
				<div class="cmeta">Commented on 25/12/2012</div>
				<p>Nulla facilisi. Sed justo dui, scelerisque ut consectetur vel, eleifend id erat. Phasellus condimentum rutrum aliquet. Quisque eu consectetur erat.</p>
				<div class="clearfix"></div>
			</li>
        </ul>
	</div>
	
	<!-- posting comment -->
	<div class="respond">
		<div class="title"><h4>Post Reply</h4></div>
        <form>
			<ul>
				<li class="field">
					<label for="name">Your Name</label>
					<input class="text input" id="name" type="text" placeholder="Enter Name">
				</li>
				<li class="field">
					<label for="email">Email address</label>
					<input class="email input" id="email" type="email" placeholder="Enter email">
				</li>
				<li class="field">
					<label for="comment">Comment</label>
					<textarea class="input textarea" id="comment" placeholder="Textarea" rows="3"></textarea>
				</li>
				
			</ul>
			<div class="medium metro rounded btn primary"><button>Submit</button></div>
	   		<div class="pretty medium default btn"><button>Reset</button></div>
		</form>                      
	</div>

	<!-- pagination -->
	<div class="row">
	   <div class="medium metro rounded btn primary icon-left icon-left-open"><a href="#">Previous Post</a></div>
	   <div class="medium metro rounded btn primary icon-right icon-right-open fright"><a href="#">Next Post</a></div>
  	</div>
	<br>
</div>

<!-- widget -->
<div class="four columns aside">
	<div class="widget">
	 <h4>Search</h4>
	    <form class="form-inline widget-search" role="form">
			<li class="append field">
				<input class="wide text input" type="text" placeholder="Type...">
				<div class="medium primary btn"><a href="#"><i class="icon-search"></i></a></div>
			</li>
	    </form>
	</div>
	<div class="widget">
	<h4>Recent Posts</h4>
		<ul>
			<li><a href="#">Condimentum</a></li>
			<li><a href="#">Etiam at</a></li>
			<li><a href="#">Fusce vel</a></li>
			<li><a href="#">Vivamus</a></li>
			<li><a href="#">Pellentesque</a></li>
		</ul>
	</div>
	<div class="widget">
	 	<h4>About</h4>
		<p>Nulla facilisi. Sed justo dui, id erat. Morbi auctor adipiscing tempor. Phasellus condimentum rutrum aliquet. Quisque eu consectetur erat. Proin rutrum, erat eget posuere semper, <em>arcu mauris posuere tortor</em>,velit at <a href="#">magna sollicitudin cursus</a> ac ultrices magna. </p>
	</div>
</div>