---
layout: page
title: Account
permalink: /account.html
---

<div class="three columns sidenav">
	{% include aside.html %}
</div>
<div class="nine columns section">
	<h4><i class="icon-user"></i> My Account</h4>
	<address>
		<!-- Your name -->
		<strong>Ashok Ramesh</strong><br>
		<!-- Address -->
		795 Folsom Ave, Suite 600<br>
		San Francisco, CA 94107<br>

		<!-- Phone number -->
		<abbr title="Phone">P:</abbr> (123) 456-7890.<br>
		<a href="mailto:#">first.last@gmail.com</a>
	</address>
	<hr>
	<h4>My Recent Purcase</h4>

	<table class="striped">
       <thead>
         <tr>
           <th>Date</th>
           <th>ID</th>
           <th>Name</th>
           <th>Price</th>
           <th>Status</th>
         </tr>
       </thead>
       <tbody>
         <tr>
           <td>25-08-12</td>
           <td>4423</td>
           <td>HTC One</td>
           <td>$530</td>
           <td>Completed</td>
         </tr>
         <tr>
           <td>15-02-12</td>
           <td>6643</td>
           <td>Sony Xperia</td>
           <td>$330</td>
           <td>Shipped</td>
         </tr>
         <tr>
           <td>14-08-12</td>
           <td>1283</td>
           <td>Nokia Asha</td>
           <td>$230</td>
           <td>Processing</td>
         </tr>                                               
       </tbody>
     </table>
</div>