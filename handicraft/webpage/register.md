---
layout: page
title: Login to Access <span>Amazing Benefits !!!</span>
permalink: register.html
---

<!-- register <section></section> -->
<div class="six columns">
	<div class="title">
		<h4>Have An Account</h4>
	</div>
	<p>
		By login you will be able to shop faster, be up to date on an order's status, and keep track of the orders you have previously made.
		<div class="medium metro rounded btn primary"><a href="#">Login</a></div>
	</p>
</div>

<!-- login section -->
<div class="six columns aside">
	<form action="#">
		<div class="title">
			<h4>Register</h4>
		</div>
		<!-- name -->
		<div class="field row">
		    <div class="three columns tright">
		      <label class="mheight" for="email"><strong>Nama</strong></label>
		    </div>
		    <div class="nine columns">
		      <input class="text input" id="email" type="email" placeholder="nama" />
		    </div>
		</div>
		<!-- email -->
		<div class="field row">
		    <div class="three columns tright">
		      <label class="mheight" for="email"><strong>Email</strong></label>
		    </div>
		    <div class="nine columns">
		      <input class="text input" id="email" type="email" placeholder="email account" />
		    </div>
		</div>
		<!-- password -->
		<div class="field row">
		    <div class="three columns tright">
		      <label class="mheight" for="pass"><strong>Password</strong></label>
		    </div>
		    <div class="nine columns">
		      <input class="text input" id="pass" type="password" placeholder="password" />
		    </div>
		</div>
		<!-- re password -->
		<div class="field row">
		    <div class="three columns tright">
		      <label class="mheight" for="pass"><strong>Re-Type Password</strong></label>
		    </div>
		    <div class="nine columns">
		      <input class="text input" id="pass" type="password" placeholder="password" />
		    </div>
		</div>
		<!-- negara -->
		<div class="field row">
		    <div class="three columns tright">
		      <label class="mheight" for="pass"><strong>Negara</strong></label>
		    </div>
		    <div class="nine columns">
		      <div class="picker full">
				<select class="form-control">
			        <option>Select Country</option>
			        <option>USA</option>
			        <option>India</option>
			        <option>Canada</option>
			        <option>UK</option>
			    </select>
				</div>
		    </div>
		</div>
		<!-- provinsi -->
		<div class="field row">
		    <div class="three columns tright">
		      <label class="mheight" for="pass"><strong>Provinsi</strong></label>
		    </div>
		    <div class="nine columns">
		      <div class="picker full">
				<select class="form-control">
			        <option>Select Country</option>
			        <option>USA</option>
			        <option>India</option>
			        <option>Canada</option>
			        <option>UK</option>
			    </select>
			</div>
		    </div>
		</div>
		<!-- kota -->
		<div class="field row">
		    <div class="three columns tright">
		      <label class="mheight" for="pass"><strong>Kota</strong></label>
		    </div>
		    <div class="nine columns">
		      <div class="picker full">
				<select class="form-control">
			        <option>Select Country</option>
			        <option>USA</option>
			        <option>India</option>
			        <option>Canada</option>
			        <option>UK</option>
			    </select>
			</div>
		    </div>
		</div>
		<!-- alamat -->
		<div class="field row">
		    <div class="three columns tright">
		      <label class="mheight" for="pass"><strong>Alamat</strong></label>
		    </div>
		    <div class="nine columns">
		     	<textarea class="input textarea" id="address" placeholder="Textarea" rows="3">address</textarea>
		    </div>
		</div>
		<!-- kode pos -->
		<div class="field row">
		    <div class="three columns tright">
		      <label class="mheight" for="pass"><strong>Kode Pos</strong></label>
		    </div>
		    <div class="nine columns">
		      <input class="text input" id="pass" type="password" placeholder="kodepos" />
		    </div>
		</div>	
		<!-- telepon -->
		<div class="field row">
		    <div class="three columns tright">
		      <label class="mheight" for="pass"><strong>Telepon</strong></label>
		    </div>
		    <div class="nine columns">
		      <input class="text input" id="pass" type="password" placeholder="telepon" />
		    </div>
		</div>		
		<!-- remember -->
		<div class="field row">
		    <div class="three columns tright">
		    </div>
		    <div class="nine columns">
		      <label class="checkbox checked" for="check1">
			    <input name="checkbox[]" id="check1" value="1" type="checkbox" checked="checked">
			    <span></span> Saya telah membaca dan menyetujui <a href="#">Syarat dan Ketentuan</a>
			  </label>
		    </div>
		</div>
		<div class="field row">
		    <div class="three columns tright">
		    </div>
		    <div class="nine columns">
		      <div class="medium metro rounded btn primary"><button>Sign In</button></div>
		   	  <div class="pretty medium default btn"><button>Reset</button></div>
		    </div>
		</div>
	</form>
</div>