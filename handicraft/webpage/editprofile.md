---
layout: page
title: Edit <span>Profile</span>
permalink: editprofile.html
---

<div class="three columns sidenav">
	{% include aside.html %}
</div>
<div class="nine columns section">
	<div class="title">
		<h4>Edit Your Profile</h4>
	</div>
	<br>

	<form action="#">
		<!-- name -->
		<div class="field row">
		    <div class="three columns tright">
		      <label class="mheight" for="text2"><strong>Name</strong></label>
		    </div>
		    <div class="nine columns">
		      <input class="wide text input" id="text2" type="text" placeholder="name" />
		    </div>
		</div>

		<!-- email -->
		<div class="field row">
		    <div class="three columns tright">
		      <label class="mheight" for="text2"><strong>Email</strong></label>
		    </div>
		    <div class="nine columns">
		      <input class="wide email input" id="text2" type="text" placeholder="email" />
		    </div>
		</div>

		<!-- phone -->
		<div class="field row">
		    <div class="three columns tright">
		      <label class="mheight" for="text2"><strong>Phone Number</strong></label>
		    </div>
		    <div class="nine columns">
		      <input class="wide text input" id="text2" type="password" placeholder="phone number" />
		    </div>
		</div>

		<!-- country -->
		<div class="field row">
		    <div class="three columns tright">
		      <label class="mheight" for="text2"><strong>Country</strong></label>
		    </div>
		    <div class="nine columns">
		    	<div class="picker">
			      	<select class="wide">
		              <option>Select Country</option>
		              <option>USA</option>
		              <option>India</option>
		              <option>Canada</option>
		              <option>UK</option>
		            </select>
			    </div>
		    </div>
		    
		</div>

		<!-- address -->
		<div class="field row">
		    <div class="three columns tright">
		      <label class="mheight" for="text2"><strong>Address</strong></label>
		    </div>
		    <div class="nine columns">
		      <textarea class="input textarea" id="message" placeholder="Textarea" rows="3"></textarea>
		    </div>
		</div>

		<!-- zipcode -->
		<div class="field row">
		    <div class="three columns tright">
		      <label class="mheight" for="text2"><strong>Zip Code</strong></label>
		    </div>
		    <div class="nine columns">
		      <input class="wide password input" id="text2" type="text" placeholder="zip code" />
		    </div>
		</div>

		<!-- username -->
		<div class="field row">
		    <div class="three columns tright">
		      <label class="mheight" for="text2"><strong>Username</strong></label>
		    </div>
		    <div class="nine columns">
		      <input class="wide password input" id="text2" type="text" placeholder="username" />
		    </div>
		</div>

		<!-- pasword -->
		<div class="field row">
		    <div class="three columns tright">
		      <label class="mheight" for="text2"><strong>Password</strong></label>
		    </div>
		    <div class="nine columns">
		      <input class="wide password input" id="text2" type="password" placeholder="password" />
		    </div>
		</div>

		<!-- term and condition -->
		<div class="field row">
		    <div class="three columns tright">
		      
		    </div>
		    <div class="nine columns">
		      <label class="checkbox" for="check2">
			    <input name="checkbox[]" id="check2" value="2" type="checkbox">
			    <span></span> Accept Terms & Conditions
			  </label>
		    </div>
		</div>
		<div class="field row">
		    <div class="three columns tright">
		      
		    </div>
		    <div class="nine columns">
		     	<div class="medium metro rounded btn primary"><button>Save Changes</button></div>
		   		<div class="pretty medium default btn"><button>Reset</button></div>
		    </div>
		</div>
	</form>
</div>