---
layout: page
title: Order <span>History</span>
permalink: orderhistory.html
---
<div class="three columns sidenav">
	{% include aside.html %}
</div>
<div class="nine columns section">
	<table>
		<thead>
		  <tr>
		    <th>Date</th>
		    <th>ID</th>
		    <th>Name</th>
		    <th>Price</th>
		    <th>Status</th>
		  </tr>
		</thead>
		<tbody>
		  <tr>
		    <td>25-08-12</td>
		    <td>4423</td>
		    <td>HTC One</td>
		    <td>$530</td>
		    <td>Completed</td>
		  </tr>
		  <tr>
		    <td>15-02-12</td>
		    <td>6643</td>
		    <td>Sony Xperia</td>
		    <td>$330</td>
		    <td>Shipped</td>
		  </tr>
		  <tr>
		    <td>14-08-12</td>
		    <td>1243</td>
		    <td>Nokia Asha</td>
		    <td>$230</td>
		    <td>Processing</td>
		  </tr>  
		  <tr>
		    <td>14-08-12</td>
		    <td>1283</td>
		    <td>Xperia Tipo</td>
		    <td>$330</td>
		    <td>Shipping</td>
		  </tr>
		  <tr>
		    <td>14-08-12</td>
		    <td>8283</td>
		    <td>Apple iPhone</td>
		    <td>$730</td>
		    <td>Processing</td>
		  </tr>
		  <tr>
		    <td>14-08-12</td>
		    <td>3283</td>
		    <td>Windows Mobile</td>
		    <td>$130</td>
		    <td>Shipped</td>
		  </tr>
		  <tr>
		    <td>14-08-12</td>
		    <td>1523</td>
		    <td>Galaxy SIII</td>
		    <td>$430</td>
		    <td>Completed</td>
		  </tr>                                                                                                             
		</tbody>
	</table>
</div>