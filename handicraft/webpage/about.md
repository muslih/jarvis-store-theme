---
layout: page
title: About
permalink: /about-us.html
---

Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas, harum. Nobis quis assumenda quibusdam eaque odio ducimus a, nihil officia dolorem, quas provident dicta similique atque labore maxime possimus exercitationem?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam autem, eveniet similique! Nisi perspiciatis odit blanditiis ipsam dolor. Ab in dolorum voluptatibus, illo amet error cum voluptatem provident porro velit.

Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat sed ipsam quam aliquam, officiis tempora expedita, quis debitis sequi reprehenderit velit aperiam facilis asperiores, est quidem, odit voluptatibus voluptate incidunt.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rerum debitis atque, eaque porro dignissimos, magni accusantium laborum quibusdam delectus omnis suscipit ipsum pariatur reprehenderit perspiciatis numquam consectetur nam nulla, eius.

<h3>Default Tabs</h3>
<section class="tabs">
    <ul class="tab-nav">
        <li><a href="#">First Tab</a></li>
        <li class="active"> <a href="#">Second Tab</a></li>
        <li><a href="#">Third Tab</a></li>
    </ul>
    <div class="tab-content">
        <p>Here's the first piece of content</p>
    </div>
    <div class="tab-content active">
        <p>My tab is active so I'll show up first! Inb4 tab 3!</p>
    </div>
    <div class="tab-content">
        <p>Don't forget about me! I'm tab 3!</p>
    </div>
</section>

<h3>Pill Tabs</h3>
<section class="pill tabs">
    <ul class="tab-nav">
        <li><a href="#">First Tab</a></li>
        <li class="active"> <a href="#">Second Tab</a></li>
        <li><a href="#">Third Tab</a></li>
    </ul>
    <div class="tab-content">
        <p>Here's the first piece of content</p>
    </div>
    <div class="tab-content active">
        <p>My tab is active so I'll show up first! Inb4 tab 3!</p>
    </div>
    <div class="tab-content">
        <p>Don't forget about me! I'm tab 3!</p>
    </div>
</section>

