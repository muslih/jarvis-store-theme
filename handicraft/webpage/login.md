---
layout: page
title: Login to Access <span>Amazing Benefits !!!</span>
permalink: login.html
---

<!-- register <section></section> -->
<div class="six columns">
	<div class="title">
		<h4>New Costumer</h4>
	</div>
	<p>
		By creating an account you will be able to shop faster, be up to date on an order's status, and keep track of the orders you have previously made.
		<div class="medium metro rounded btn primary"><input type="submit" value="Submit"></div>
	</p>
</div>

<!-- login section -->
<div class="six columns aside">
	<form action="#">
		<div class="title">
			<h4>Login</h4>
		</div>
		<div class="field row">
		    <div class="three columns tright">
		      <label class="mheight" for="email"><strong>Email</strong></label>
		    </div>
		    <div class="nine columns">
		      <input class="text input" id="email" type="email" placeholder="email account" />
		    </div>
		</div>
		<div class="field row">
		    <div class="three columns tright">
		      <label class="mheight" for="pass"><strong>Password</strong></label>
		    </div>
		    <div class="nine columns">
		      <input class="text input" id="pass" type="password" placeholder="password" />
		    </div>
		</div>
		<div class="field row">
		    <div class="three columns tright">
		    </div>
		    <div class="nine columns">
		      <label class="checkbox checked" for="check1">
			    <input name="checkbox[]" id="check1" value="1" type="checkbox" checked="checked">
			    <span></span> Rememberme <a href="#" class="tright fright">Forget your password? </a>
			  </label>
		    </div>
		</div>
		<div class="field row">
		    <div class="three columns tright">
		    </div>
		    <div class="nine columns">
		      <div class="medium metro rounded btn primary"><button>Sign In</button></div>
		   	  <div class="pretty medium default btn"><button>Reset</button></div>
		    </div>
		</div>
	</form>
</div>