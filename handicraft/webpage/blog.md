---
layout: page
title: Blog
permalink: /blog.html
---

<div class="eight columns section">
	<div class="row">
		<article class="three columns">
			<div class="image photo">
				<img src="./public/img/photo.jpg" alt="">
			</div>
		</article>
		<article class="nine columns">
			<h3><a href="#">Mauris est et turpis.</a></h3>
			<div class="meta">
                <i class="icon-calendar"></i> 26-2-2012 <i class="icon-user"></i> Admin <i class="icon-folder-open"></i> <a href="#">General</a> <span class="pull-right"><i class="icon-comment"></i> <a href="#">2 Comments</a></span>
            </div>
			<p>Proin elit arcu, <em class="ttip" data-tooltip="Another tooltip!">rutrum</em> commodo, vehicula tempus, commodo a, risus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est voluptatibus reiciendis, porro officiis. Rerum veritatis optio eius? Deserunt iusto sequi libero illum inventore molestiae assumenda minima, placeat, laborum esse reiciendis. </p>
			<div class="pretty medium default btn"><a href="#">Read more <i class="icon-right-open"></i> </a></div>
		</article>
	</div>
	<hr>
	<div class="row">
		<article class="three columns">
			<div class="image photo">
				<img src="./public/img/photo.jpg" alt="">
			</div>
		</article>
		<article class="nine columns">
			<h3><a href="#">Mauris est et turpis.</a></h3>
			<div class="meta">
                <i class="icon-calendar"></i> 26-2-2012 <i class="icon-user"></i> Admin <i class="icon-folder-open"></i> <a href="#">General</a> <span class="pull-right"><i class="icon-comment"></i> <a href="#">2 Comments</a></span>
            </div>
			<p>Proin elit arcu, <em class="ttip" data-tooltip="Another tooltip!">rutrum</em> commodo, vehicula tempus, commodo a, risus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est voluptatibus reiciendis, porro officiis. Rerum veritatis optio eius? Deserunt iusto sequi libero illum inventore molestiae assumenda minima, placeat, laborum esse reiciendis. </p>
			<div class="pretty medium default btn"><a href="#">Read more <i class="icon-right-open"></i> </a></div>
		</article>
	</div>
	<hr>
	<div class="row">
		<article class="three columns">
			<div class="image photo">
				<img src="./public/img/photo.jpg" alt="">
			</div>
		</article>
		<article class="nine columns">
			<h3><a href="#">Mauris est et turpis.</a></h3>
			<div class="meta">
                <i class="icon-calendar"></i> 26-2-2012 <i class="icon-user"></i> Admin <i class="icon-folder-open"></i> <a href="#">General</a> <span class="pull-right"><i class="icon-comment"></i> <a href="#">2 Comments</a></span>
            </div>
			<p>Proin elit arcu, <em class="ttip" data-tooltip="Another tooltip!">rutrum</em> commodo, vehicula tempus, commodo a, risus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est voluptatibus reiciendis, porro officiis. Rerum veritatis optio eius? Deserunt iusto sequi libero illum inventore molestiae assumenda minima, placeat, laborum esse reiciendis. </p>
			<div class="pretty medium default btn"><a href="#">Read more <i class="icon-right-open"></i> </a></div>
		</article>
	</div>
	<hr>
	<div class="row">
		<article class="three columns">
			<div class="image photo">
				<img src="./public/img/photo.jpg" alt="">
			</div>
		</article>
		<article class="nine columns">
			<h3><a href="#">Mauris est et turpis.</a></h3>
			<div class="meta">
                <i class="icon-calendar"></i> 26-2-2012 <i class="icon-user"></i> Admin <i class="icon-folder-open"></i> <a href="#">General</a> <span class="pull-right"><i class="icon-comment"></i> <a href="#">2 Comments</a></span>
            </div>
			<p>Proin elit arcu, <em class="ttip" data-tooltip="Another tooltip!">rutrum</em> commodo, vehicula tempus, commodo a, risus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est voluptatibus reiciendis, porro officiis. Rerum veritatis optio eius? Deserunt iusto sequi libero illum inventore molestiae assumenda minima, placeat, laborum esse reiciendis. </p>
			<div class="pretty medium default btn"><a href="#">Read more <i class="icon-right-open"></i> </a></div>
		</article>
	</div>
	<hr>
	<div class="row">
		<li class="default badge"><a href="#"><i class="icon-left-open"></i></a></li>
		<li class="info badge"><a href="#">1</a></li>
		<li class="default badge"><a href="#">2</a></li>
		<li class="default badge"><a href="#">3</a></li>
		<li class="default badge"><a href="#">4</a></li>
		<li class="default badge"><a href="#">5</a></li>
		<li class="default badge"><a href="#"><i class="icon-right-open"></i></a></li>
	</div>
	<br>
</div>
<div class="four columns aside">
	<div class="widget">
	 <h4>Search</h4>
	    <form class="form-inline widget-search" role="form">
			<li class="append field">
				<input class="wide text input" type="text" placeholder="Type...">
				<div class="medium primary btn"><a href="#"><i class="icon-search"></i></a></div>
			</li>
	    </form>
	</div>

	<div class="widget">
	<h4>Recent Posts</h4>
		<ul>
			<li><a href="#">Condimentum</a></li>
			<li><a href="#">Etiam at</a></li>
			<li><a href="#">Fusce vel</a></li>
			<li><a href="#">Vivamus</a></li>
			<li><a href="#">Pellentesque</a></li>

		</ul>
	</div>

	<div class="widget">
	 	<h4>About</h4>
		<p>Nulla facilisi. Sed justo dui, id erat. Morbi auctor adipiscing tempor. Phasellus condimentum rutrum aliquet. Quisque eu consectetur erat. Proin rutrum, erat eget posuere semper, <em>arcu mauris posuere tortor</em>,velit at <a href="#">magna sollicitudin cursus</a> ac ultrices magna. </p>
	</div>
</div>