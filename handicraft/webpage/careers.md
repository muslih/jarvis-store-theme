---
layout: page
title: Careers  <span>Subtext heading</span>
permalink: careers.html
---
<br>

<section class="tabs">
    <ul class="tab-nav">
        <li><a href="#" >Home</a></li>
        <li class="active"> <a href="#">Profile</a></li>
        <li><a href="#">Messages</a></li>
        <li><a href="#">Settings</a></li>
    </ul>
    <div class="tab-content">
        <!-- Content -->
      	<h3>Web Designer - 2 Nos</h3>
      	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vulputate eros nec odio egestas in dictum nisi vehicula. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse porttitor luctus imperdiet. <a href="#">Praesent ultricies</a> enim ac ipsum aliquet pellentesque. Nullam justo nunc, dignissim at convallis posuere, sodales eu orci. Duis a risus sed dolor placerat semper quis in urna. Nam risus magna, fringilla sit amet blandit viverra, dignissim eget est. Ut <strong>commodo ullamcorper risus nec</strong> mattis. Fusce imperdiet ornare dignissim. Donec aliquet convallis tortor, et placerat quam posuere posuere. Morbi tincidunt posuere turpis eu laoreet. Nulla facilisi. Aenean at massa leo. Vestibulum in varius arcu.</p>
      	<ul class="square">
         <li>Etiam adipiscing posuere justo, nec iaculis justo dictum non</li>
         <li>Cras tincidunt mi non arcu hendrerit eleifend</li>
         <li>Aenean ullamcorper justo tincidunt justo aliquet et lobortis diam faucibus</li>
         <li>Maecenas hendrerit neque id ante dictum mattis</li>
         <li>Vivamus non neque lacus, et cursus tortor</li>
      	</ul>
      	<div class="medium metro rounded btn primary"><button>Apply Online</button></div>
    </div>
    <div class="tab-content active">
        <h3>PHP Developers - 3 Nos</h3>
        <p><a href="#">Praesent ultricies</a> enim ac ipsum aliquet pellentesque. Nullam justo nunc, dignissim at convallis posuere, sodales eu orci. Duis a risus sed dolor placerat semper quis in urna. Nam risus magna, fringilla sit amet blandit viverra, dignissim eget est. Ut <strong>commodo ullamcorper risus nec</strong> mattis. Fusce imperdiet ornare dignissim. Donec aliquet convallis tortor, et placerat quam posuere posuere. Morbi tincidunt posuere turpis eu laoreet. Nulla facilisi. Aenean at massa leo. Vestibulum in varius arcu.</p>
         <div class="medium metro rounded btn primary"><button>Apply Online</button></div>                    
    </div>
    <div class="tab-content">
        <h3>Graphics Designer - 1 Nos</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vulputate eros nec odio egestas in dictum nisi vehicula. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse porttitor luctus imperdiet. <a href="#">Praesent ultricies</a> enim ac ipsum aliquet pellentesque. Nullam justo nunc, dignissim at convallis posuere, sodales eu orci.</p>
        <ul class="disc">
             <li>Etiam adipiscing posuere justo, nec iaculis justo dictum non</li>
             <li>Cras tincidunt mi non arcu hendrerit eleifend</li>
             <li>Aenean ullamcorper justo tincidunt justo aliquet et lobortis diam faucibus</li>
             <li>Maecenas hendrerit neque id ante dictum mattis</li>
             <li>Vivamus non neque lacus, et cursus tortor</li>
        </ul>
        <div class="medium metro rounded btn primary"><button>Apply Online</button></div> 
    </div>
    <div class="tab-content">
    	<h3>Database Designer - 1 Nos</h3>
		<p> Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse porttitor luctus imperdiet. <a href="#">Praesent ultricies</a> enim ac ipsum aliquet pellentesque. Nullam justo nunc, dignissim at convallis posuere, sodales eu orci. Duis a risus sed dolor placerat semper quis in urna. Nam risus magna, fringilla sit amet blandit viverra, dignissim eget est. Ut <strong>commodo ullamcorper risus nec</strong> mattis. Fusce imperdiet ornare dignissim. </p>
		<ul class="disc">
			 <li>Fusce imperdiet ornare dignissim</li>
			 <li>Cras tincidunt mi non arcu hendrerit eleifend</li>
			 <li>Aenean ullamcorper justo tincidunt justo aliquet et lobortis diam faucibus</li>
			 <li>Maecenas hendrerit neque id ante dictum mattis</li>
			 <li>Vivamus non neque lacus, et cursus tortor</li>
		</ul>
		<div class="medium metro rounded btn primary"><button>Apply Online</button></div> 
    </div>
</section>