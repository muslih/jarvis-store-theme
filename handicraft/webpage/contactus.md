---
layout: page
title: <i class="icon-mail"></i> Contact <span>Us</span>
permalink: /contactus.html
---

<div class="row section">
	<div class="six columns">
		<div class="respond">
			<div class="title">
				<h4>Contact Form</h4>
			</div>
			<form>
				<ul>
					<li class="field">
						<label for="name">Your Name</label>
						<input class="text input" id="name" type="text" placeholder="Enter Name">
					</li>
					<li class="field">
						<label for="email">Email address</label>
						<input class="email input" id="email" type="email" placeholder="Enter email">
					</li>
					<li class="field">
						<label for="message">Message</label>
						<textarea class="input textarea" id="message" placeholder="Textarea" rows="3"></textarea>
					</li>
					<li class="field">
						<label class="checkbox" for="checkbox1">
								<input name="checkbox1" type="checkbox" id="checkbox1">
								<span></span> Important
							</label>
					</li>
				</ul>
				<div class="medium metro rounded btn primary"><button>Submit</button></div>
		   		<div class="pretty medium default btn"><button>Reset</button></div>
			</form>     
		</div>
	</div>
	<div class="six columns">
		<div class="title">
			<h4>Address</h4>
		</div>
		<div class="address">
           <address>
              <!-- Company name -->
              <h6>Responsive Web, Inc.</h6>
              <!-- Address -->
              795 Folsom Ave, Suite 600<br>
              San Francisco, CA 94107<br>
              <!-- Phone number -->
              <abbr title="Phone">P:</abbr> (123) 456-7890.
           </address>
            
           <address>
              <!-- Name -->
              <h6>Full Name</h6>
              <!-- Email -->
              <a href="mailto:#">first.last@gmail.com</a>
           </address>
           
       </div>
       <iframe height="250" width="100%" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.co.in/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Google+India+Bangalore,+Bennigana+Halli,+Bangalore,+Karnataka&amp;aq=0&amp;oq=google+&amp;sll=9.930582,78.12303&amp;sspn=0.192085,0.308647&amp;ie=UTF8&amp;hq=Google&amp;hnear=Bennigana+Halli,+Bangalore,+Bengaluru+Urban,+Karnataka&amp;t=m&amp;ll=12.993518,77.660294&amp;spn=0.012545,0.036006&amp;z=15&amp;output=embed"></iframe>

	</div>
</div>
<br>