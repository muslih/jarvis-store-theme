---
layout: page
title: Checkout <span>Subtext header</span>
permalink: /checkout.html
---

<div class="row checkout" >
	<div class="twelve columns">
		<div class="title">
			<h4>Shipping & Billing Details</h4>
		</div>

		<form action="#">
			<div class="field">
				<label class="inline" for="name">Name</label>
				<input class="text input" id="name" name="name" type="text" placeholder="Name">
			</div>
			<div class="field">
				<label class="inline" for="email">Email</label>
				<input class="email input" id="email" name="email" type="text" placeholder="Email">
			</div>
			<div class="field">
				<label class="inline" for="phone">Phone </label>
				<input class="email input" id="phone" name="phone" type="text" placeholder="Phone">
			</div>
			<div class="field">
				<label class="block" for="country">Country </label>
				<div class="picker full">
					<select class="form-control">
				        <option>Select Country</option>
				        <option>USA</option>
				        <option>India</option>
				        <option>Canada</option>
				        <option>UK</option>
				    </select>
				</div>
			</div>
			<div class="field">
				<label class="inline" for="address">Address </label>
				<textarea class="input textarea" id="address" placeholder="Textarea" rows="3">address</textarea>
			</div>
			<div class="field">
				<label class="inline" for="zip">Zip Code </label>
				<input class="email input" id="zip" name="zip" type="text" placeholder="zip">
			</div>
		
	
			<div class="title">
				<h4>Payment Details</h4>
			</div>

			<div class="field">
				<label class="block" for="payment">Payment Method </label>
				<div class="picker">
					<select class="form-control">
				        <option>Payment Method</option>
	                    <option>Credit Card</option>
	                    <option>Debit Card</option>
	                    <option>Internet Banking</option>
	                    <option>Check</option>
				    </select>
				</div>
			</div>
			<div class="field">
				<label class="checkbox" for="checkbox1">
					<input name="checkbox1" type="checkbox" id="checkbox1">
					<span></span> Accept Terms & Conditions
				</label>
			</div>
			<div class="medium metro rounded btn primary"><button>Submit</button></div>
		   	<div class="pretty medium default btn"><button>Reset</button></div>
		</form>
	</div>

</div>
