---
layout: page
title: Cart
permalink: /cart.html
---

<table class="rounded">
	<thead>
		<tr>
			<th>No</th>
			<th>Name</th>
			<th>Image</th>
			<th>Quantity</th>
			<th>Unit Price</th>
			<th>Total</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>1</td>
			<td><a href="#">HTC One</a></td>
			<td>
				<img src="./public/img/photo.jpg" width="50">
			</td>
			<td>
				<div class=" field">
				    <input class="normal number input" type="number" placeholder="1" />
				</div>
				<div class="small oval secondary btn"><a href="#"><i class="icon-arrows-ccw"></i></a></div>
				<div class="small oval danger btn"><a href="#"><i class="icon-trash"></i></a></div>
			</td>
			<td>$100</td>
			<td>$200</td>
		</tr>
		<tr>
			<td>2</td>
			<td><a href="#">Lorem ipsum dolor </a></td>
			<td>
				<img src="./public/img/photo.jpg" width="50">
			</td>
			<td>
				<div class=" field">
				    <input class="normal number input" type="number" placeholder="1" />
				</div>
				<div class="small oval secondary btn"><a href="#"><i class="icon-arrows-ccw"></i></a></div>
				<div class="small oval danger btn"><a href="#"><i class="icon-trash"></i></a></div>
			</td>
			<td>$100</td>
			<td>$200</td>
		</tr>
		<tr>
			<td>3</td>
			<td><a href="#">HTC One</a></td>
			<td>
				<img src="./public/img/photo.jpg" width="50">
			</td>
			<td>
				<div class=" field">
				    <input class="normal number input" type="number" placeholder="1" />
				</div>
				<div class="small oval secondary btn"><a href="#"><i class="icon-arrows-ccw"></i></a></div>
				<div class="small oval danger btn"><a href="#"><i class="icon-trash"></i></a></div>
			</td>
			<td>$100</td>
			<td>$200</td>
		</tr>
		<tr>
			<td colspan="6">
				<div class="row">
					<div class="six columns">
						<h4>Discount Coupon</h4>
						<div class="append field">
				            <input class="wide text input" type="text" placeholder="coupon" />
				            <div class="medium primary btn"><a href="#">Apply</a></div>
				        </div>
					</div>
					<div class="six columns">
						<h4>Gift Coupon</h4>
						<div class="append field">
				            <input class="wide text input" type="text" placeholder="coupon" />
				            <div class="medium primary btn"><a href="#">Apply</a></div>
				        </div>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="5"><strong>Sub Total</strong> </td>
			<td>$500</td>
		</tr>
		<tr>
			<td colspan="5"><strong>Total</strong> </td>
			<td>$450</td>
		</tr>
	</tbody>
</table>

<div class="row">
	<div class="twelve columns">
		<div class="medium btn primary metro rounded icon-left entypo icon-tag"><a href="#">Continue Shopping</a></div>
		<div class="medium btn secondary metro rounded icon-left entypo icon-basket"><a href="checkout.html">Checkout</a></div>
	</div>
</div>
<br>