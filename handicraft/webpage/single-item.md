---
layout: page
title: Laptops & Desktops <span>Subtext for header</span>
permalink: single-item.html
---

<div class="row">
		<div class="three columns sidenav">
		{% include aside_product.html %}
	</div>

	<div class="nine columns section">
		<ul class="breadcrumbs">
		  <li><a href="#">Home</a></li>
		  <li><a href="#">Features</a></li>
		  <li class="unavailable"><a href="#">Gene Splicing</a></li>
		  <li class="current"><a href="#">Cloning</a></li>
		</ul>
		<div class="row">
			<div class="five columns section">
				<div class="image photo product">
					<a href="#">
			          <img src="./public/img/photo.jpg" alt="New">
			        </a>
					<div class="product-detail">
			          <span class="sale">Sale!</span>            
			        </div>
				</div>
			</div>
			<div class="seven columns section productdetail">
				<div class="title">
					<h4>Product Title</h4>
				</div>
				<p class="price">
					<del><span class="amount">IDR.350K</span></del>
					<ins><span class="amount">IDR.300K</span></ins>
				</p>

				<div class="desc">
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius, quae aspernatur eligendi laudantium pariatur dicta deserunt, fugiat praesentium possimus, libero sint. Vel, quas. Tempore perferendis sunt, perspiciatis maiores, laudantium aliquam.
					</p>
				</div>
				<form class="cart" method="post" enctype="multipart/form-data">
					<div class="picker ">
						<select class="form-control">
					        <option>Select Type</option>
					        <option>USA</option>
					        <option>India</option>
					        <option>Canada</option>
					        <option>UK</option>
					    </select>
					</div>
					<li class="append field">
						<input class="narrow text input" type="number" step="1" min="1" placeholder="2" >
						<div class="medium primary btn icon-left icon-basket"><a href="#">Add to cart</a></div>
					</li>
				 	<a href="#">+ Add To Wishlist</a>
				</form>
			</div>
		
		</div>
		<div class="row">
			<section class="tabs">
				<ul class="tab-nav">
					<li class="active"><a href="#">Description</a></li>
					<li class=""><a href="#">Specs</a></li>
					<li class=""><a href="#">Review (5)</a></li>
				</ul>
				<div class="tab-content">
					<p>Proin elit arcu, rutrum commodo, vehicula tempus, commodo a, risus. Curabitur nec arcu. Donec sollicitudin mi sit amet mauris. Nam elementum quam ullamcorper ante. Etiam aliquet massa et lorem. Mauris dapibus lacus auctor risus. Aenean tempor ullamcorper leo. Vivamus sed magna quis ligula eleifend adipiscing. Duis orci. Aliquam sodales tortor vitae ipsum. Aliquam nulla. Duis aliquam molestie erat. Ut et mauris vel pede varius sollicitudin. Sed ut dolor nec orci tincidunt interdum. Phasellus ipsum. Nunc tristique tempus lectus.</p>
					<div class="medium primary btn pull_left"><a href="">My cool button</a></div>
				</div>
				<div class="tab-content active">
					<p>Morbi tincidunt, dui sit amet facilisis feugiat, odio metus gravida ante, ut pharetra massa metus id nunc. Duis scelerisque molestie turpis. Sed fringilla, massa eget luctus malesuada, metus eros molestie lectus, ut tempus eros massa ut dolor. Aenean aliquet fringilla sem. Suspendisse sed ligula in ligula suscipit aliquam. Praesent in eros vestibulum mi adipiscing adipiscing. Morbi facilisis. Curabitur ornare consequat nunc. Aenean vel metus. Ut posuere viverra nulla. Aliquam erat volutpat. Pellentesque convallis. Maecenas feugiat, tellus pellentesque pretium posuere, felis lorem euismod felis, eu ornare leo nisi vel felis. Mauris consectetur tortor et purus.</p>
				</div>
				<div class="tab-content">
					<p>Mauris eleifend est et turpis. Duis id erat. Suspendisse potenti. Aliquam vulputate, pede vel vehicula accumsan, mi neque rutrum erat, eu congue orci lorem eget lorem. Vestibulum non ante. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce sodales. Quisque eu urna vel enim commodo pellentesque. Praesent eu risus hendrerit ligula tempus pretium. Curabitur lorem enim, pretium nec, feugiat nec, luctus a, lacus.</p>
					<p>Duis cursus. Maecenas ligula eros, blandit nec, pharetra at, semper at, magna. Nullam ac lacus. Nulla facilisi. Praesent viverra justo vitae neque. Praesent blandit adipiscing velit. Suspendisse potenti. Donec mattis, pede vel pharetra blandit, magna ligula faucibus eros, id euismod lacus dolor eget odio. Nam scelerisque. Donec non libero sed nulla mattis commodo. Ut sagittis. Donec nisi lectus, feugiat porttitor, tempor ac, tempor vitae, pede. Aenean vehicula velit eu tellus interdum rutrum. Maecenas commodo. Pellentesque nec elit. Fusce in lacus. Vivamus a libero vitae lectus hendrerit hendrerit.</p>
				</div>
			</section>
		</div>
		<hr>
		<div class="row">
			<div class="title">
				<h4>Related Product</h4>
			</div>
			<div class="row">
				<!-- product section -->
				<div class="three columns image photo product">
					<!-- product image -->
					<a href="single-item.html">
					  <img src="./public/img/photo.jpg" alt="New">
					</a>
					<!-- product detail -->
					<div class="product-detail">
					  <h4 class="title"><a href="single-item.html">Product Title</a></h4>
					  <span class="sale">Sale!</span>
					  <span class="price"><del><span class="amount">IDR.490K</span></del> <ins><span class="amount">IDR. 390K</span></ins></span>
					  <div class="medium oval btn primary default">
					    <a href="single-item.html">Add Cart</a>
					  </div>            
					</div>
				</div>
				<!-- product section -->
				<div class="three columns image photo product">
					<!-- product image -->
					<a href="single-item.html">
					  <img src="./public/img/photo.jpg" alt="New">
					</a>
					<!-- product detail -->
					<div class="product-detail">
					  <h4 class="title"><a href="single-item.html">Product Title</a></h4>
					  <span class="sale">Sale!</span>
					  <span class="price"><del><span class="amount">IDR.490K</span></del> <ins><span class="amount">IDR. 390K</span></ins></span>
					  <div class="medium oval btn primary default">
					    <a href="single-item.html">Add Cart</a>
					  </div>            
					</div>
				</div>
				<!-- product section -->
				<div class="three columns image photo product">
					<!-- product image -->
					<a href="single-item.html">
					  <img src="./public/img/photo.jpg" alt="New">
					</a>
					<!-- product detail -->
					<div class="product-detail">
					  <h4 class="title"><a href="single-item.html">Product Title</a></h4>
					  <span class="sale">Sale!</span>
					  <span class="price"><del><span class="amount">IDR.490K</span></del> <ins><span class="amount">IDR. 390K</span></ins></span>
					  <div class="medium oval btn primary default">
					    <a href="single-item.html">Add Cart</a>
					  </div>            
					</div>
				</div>
				<!-- product section -->
				<div class="three columns image photo product">
					<!-- product image -->
					<a href="single-item.html">
					  <img src="./public/img/photo.jpg" alt="New">
					</a>
					<!-- product detail -->
					<div class="product-detail">
					  <h4 class="title"><a href="single-item.html">Product Title</a></h4>
					  <span class="sale">Sale!</span>
					  <span class="price"><del><span class="amount">IDR.490K</span></del> <ins><span class="amount">IDR. 390K</span></ins></span>
					  <div class="medium oval btn primary default">
					    <a href="single-item.html">Add Cart</a>
					  </div>            
					</div>
				</div> 
			</div>     
		</div>
	</div>

</div>

