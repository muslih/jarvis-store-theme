---
layout: page
title:  Laptops & Desktops <span>Subtext for header</span>
permalink: items.html
---

<div class="three columns sidenav">
	{% include aside_product.html %}
</div>
<div class="nine columns section">
	<ul class="breadcrumbs">
	  <li><a href="#">Home</a></li>
	  <li><a href="#">Features</a></li>
	  <li class="unavailable"><a href="#">Gene Splicing</a></li>
	  <li class="current"><a href="#">Cloning</a></li>
	</ul>
	
    <div class="row">
	    <!-- product section -->
	    <div class="four columns image photo product">
	      	<!-- product image -->
	  		<a href="#">
	  			<img src="./public/img/photo.jpg" alt="New">
	  		</a>
	      	<!-- product detail -->
	      	<div class="product-detail">
				<h4 class="title"><a href="#">Product Title</a></h4>
				<span class="sale">Sale!</span>
				<span class="price"><del><span class="amount">IDR.490K</span></del> <ins><span class="amount">IDR. 390K</span></ins></span>
				<div class="medium oval btn primary default">
					<a href="#">Add Cart</a>
				</div>						
			</div>
	    </div>

	    <!-- product section -->
	    <div class="four columns image photo product">
	      	<!-- product image -->
	  		<a href="#">
	  			<img src="./public/img/photo.jpg" alt="New">
	  		</a>
	      	<!-- product detail -->
	      	<div class="product-detail">
				<h4 class="title"><a href="#">Product Title</a></h4>
				<span class="sale">Sale!</span>
				<span class="price"><del><span class="amount">IDR.490K</span></del> <ins><span class="amount">IDR. 390K</span></ins></span>
				<div class="medium oval btn primary default">
					<a href="#">Add Cart</a>
				</div>						
			</div>
	    </div>

	    <!-- product section -->
	    <div class="four columns image photo product">
	      	<!-- product image -->
	  		<a href="#">
	  			<img src="./public/img/photo.jpg" alt="New">
	  		</a>
	      	<!-- product detail -->
	      	<div class="product-detail">
				<h4 class="title"><a href="#">Product Title</a></h4>
				<span class="price"><del><span class="amount">IDR.490K</span></del> <ins><span class="amount">IDR. 390K</span></ins></span>
				<div class="medium oval btn primary default">
					<a href="#">Add Cart</a>
				</div>						
			</div>
	    </div>
      
    </div>
    <div class="row">
	    <!-- product section -->
	    <div class="four columns image photo product">
	      	<!-- product image -->
	  		<a href="#">
	  			<img src="./public/img/photo.jpg" alt="New">
	  		</a>
	      	<!-- product detail -->
	      	<div class="product-detail">
				<h4 class="title"><a href="#">Product Title</a></h4>
				<span class="sale">Sale!</span>
				<span class="price"><del><span class="amount">IDR.490K</span></del> <ins><span class="amount">IDR. 390K</span></ins></span>
				<div class="medium oval btn primary default">
					<a href="#">Add Cart</a>
				</div>						
			</div>
	    </div>

	    <!-- product section -->
	    <div class="four columns image photo product">
	      	<!-- product image -->
	  		<a href="#">
	  			<img src="./public/img/photo.jpg" alt="New">
	  		</a>
	      	<!-- product detail -->
	      	<div class="product-detail">
				<h4 class="title"><a href="#">Product Title</a></h4>
				
				<span class="price"><del><span class="amount">IDR.490K</span></del> <ins><span class="amount">IDR. 390K</span></ins></span>
				<div class="medium oval btn primary default">
					<a href="#">Add Cart</a>
				</div>						
			</div>
	    </div>

	    <!-- product section -->
	    <div class="four columns image photo product">
	      	<!-- product image -->
	  		<a href="#">
	  			<img src="./public/img/photo.jpg" alt="New">
	  		</a>
	      	<!-- product detail -->
	      	<div class="product-detail">
				<h4 class="title"><a href="#">Product Title</a></h4>
				<span class="sale">Sale!</span>
				<span class="price"><del><span class="amount">IDR.490K</span></del> <ins><span class="amount">IDR. 390K</span></ins></span>
				<div class="medium oval btn primary default">
					<a href="#">Add Cart</a>
				</div>						
			</div>
	    </div>
      
    </div>
</div>
