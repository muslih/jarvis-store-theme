---
layout: page
title: <i class="icon-basket"></i> We received payment <span>Successfully...</span>
permalink: /confirmation.html
---

<div class="row">
	<p>
		Thanks for buying from Jarvis-Store
	</p>
	
	<p>
		Your Order #id is <strong>43454354</strong> . Use this for further communication.
	</p>
	<br>
</div>

